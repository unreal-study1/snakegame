#pragma once

#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"

#include "Interactable.h"
#include "LevelDoorKeyElement.generated.h"

class AFloorGridFragment;

struct FBox;
class UBoxComponent;
class UMaterialInterface;
class USceneComponent;
class UStaticMesh;
class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ALevelDoorKeyElement : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	ALevelDoorKeyElement ();
	~ALevelDoorKeyElement () override = default;

	UFUNCTION(BlueprintSetter, meta = (ToolTip = "Sets scale along Z-axis for the interactive component.\nScales for X- and Y-axes are set automatically using the visual mesh.\n\nThe arg will be clamped to 0"))
	void SetInteractiveComponentWorldScaleZ (const float Scale);

	UFUNCTION(BlueprintSetter)
	void SetVisualMesh (
		UPARAM(meta = (DisplayName = "Mesh"))
		UStaticMesh *MeshPtr);

	UFUNCTION(BlueprintSetter, meta = (ToolTip = "Sets material of the visual mesh for the 'Idle' state"))
	void SetIdleStateMaterial (
		UPARAM(meta = (DisplayName = "Material"))
		UMaterialInterface *MaterialPtr);
	UFUNCTION(BlueprintSetter, meta = (ToolTip = "Sets material of the visual mesh for the 'Pressed' state"))
	void SetPressedStateMaterial (
		UPARAM(meta = (DisplayName = "Material"))
		UMaterialInterface *MaterialPtr);
	UFUNCTION(BlueprintSetter, meta = (ToolTip = "Sets material of the visual mesh for the 'Entire key activated' state"))
	void SetEntireKeyActivatedStateMaterial (
		UPARAM(meta = (DisplayName = "Material"))
		UMaterialInterface *MaterialPtr);

	UFUNCTION(BlueprintSetter, meta = (ToolTip = "Setter for the property\n\"Actor Size to Floor Grid Cell Size Ratio in 'Idle' State\".\n\nThe arg will be clamped to [0..1]"))
	void SetActorSizeToFloorGridCellSizeRatioInIdleState (const float Ratio);
	UFUNCTION(BlueprintSetter, meta = (ToolTip = "Setter for the property\n\"Actor Size to Floor Grid Cell Size Ratio in 'Pressed' State\".\n\nThe arg will be clamped to [0..1]"))
	void SetActorSizeToFloorGridCellSizeRatioInPressedState (const float Ratio);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, meta = (ToolTip = "Only when the main element of a key is pressed,\nthat (whole) key is checked for activation"))
	void MarkAsMain ();
	void MarkAsMain_Implementation ();

	UFUNCTION(BlueprintCallable)
	inline bool IsMain () const noexcept;

	UFUNCTION(BlueprintCallable, meta = (ToolTip = "It does nothing if 'Floor Grid' is empty"))
	void SnapToFloorGrid (
		UPARAM(meta = (DisplayName = "Floor Grid"))
		const AFloorGridFragment *GridPtr);

	inline bool IsInPressedState () const noexcept;

	void EnterEntireKeyActivatedState ();
	void LeaveEntireKeyActivatedState ();

public:
	UPROPERTY(EditAnywhere, BlueprintSetter = SetInteractiveComponentWorldScaleZ, meta = (DisplayName = "Interactive Component Scale Along Z", ClampMin = 0, ToolTip = "It specifies absolute scale along Z-axis for the interactive component of this key element"))
	float m_InteractiveComponentWorldScaleZ = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintSetter = SetVisualMesh, meta = (DisplayName = "Visual mesh", ToolTip = "It specifies how this key element will look to the player.\n\nThis mesh is *never* used for interaction with the player"))
	UStaticMesh *m_VisualMeshPtr = nullptr;

	UPROPERTY(EditAnywhere, BlueprintSetter = SetIdleStateMaterial, meta = (DisplayName = "Idle State Material", ToolTip = "Material for the 'Idle' state"))
	UMaterialInterface *m_IdleStateMaterialPtr = nullptr;
	UPROPERTY(EditAnywhere, BlueprintSetter = SetPressedStateMaterial, meta = (DisplayName = "Pressed State Material", ToolTip = "Material for the 'Pressed' state"))
	UMaterialInterface *m_PressedStateMaterialPtr = nullptr;
	UPROPERTY(EditAnywhere, BlueprintSetter = SetEntireKeyActivatedStateMaterial, meta = (DisplayName = "Entire key activated State Material", ToolTip = "Material for the 'Entire key activated' state"))
	UMaterialInterface *m_EntireKeyActivatedStateMaterialPtr = nullptr;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Actor Size to Floor Grid Cell Size Ratio\nin 'Idle' State", ClampMin = 0, ClampMax = 1, ToolTip = "It specifies how much area this key element will occupy\nin 'Idle' state after SnapToFloorGrid() is called"))
	float m_ActorSizeToFloorGridCellSizeRatioInIdleState = 0.8f;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Actor Size to Floor Grid Cell Size Ratio\nin 'Pressed' State", ClampMin = 0, ClampMax = 1, ToolTip = "It specifies how much area this key element will occupy\nin 'Pressed' (and 'Entire key activated') state after SnapToFloorGrid() is called"))
	float m_ActorSizeToFloorGridCellSizeRatioInPressedState = 1.0f;

protected:
	void PostRegisterAllComponents () override;

	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

	void StartInteraction (AActor *InteractorPtr) override;
	void EndInteraction (AActor *InteractorPtr) override;

#if WITH_EDITOR
	// note: it is called when scalar property, marked with UPROPERTY, changes in the editor
	void PostEditChangeProperty (FPropertyChangedEvent &Event) override;
#endif

private:
	enum class State
	{
		Idle,
		Pressed,
		EntireKeyActivated
	};

private:
	static FBox ComputeBoundingBoxInUu (const USceneComponent &Component);

private:
	void UpdateUnscaledSizeOfInteractiveComponent ();
	void UpdateInteractiveComponentTransformAlongZaxis ();

	FBox GetBoundingBoxInUu () const;

	void SetState (const State State);

	template<State State>
	inline UMaterialInterface*& StateMaterialPtr () noexcept;

	template<State State>
	void SetStateMaterial (UMaterialInterface *MaterialPtr);

	UMaterialInterface* GetMaterialPtrForCurrentState () const noexcept;

	// note: it also adjusts sizes and the location of the interactive component,
	//       because the interactive component is attached to the visual component
	void UpdateMeshOfVisualComponent ();

	void UpdateMaterialOfVisualComponent ();

	// note: it does nothing if 'm_SnappedFloorGridCellHalfSizeInUu' is invalid
	void AdjustXySizeToFloorGridCell ();

private:
	UBoxComponent *m_InteractiveComponentPtr = nullptr; // it (and only it) is used for interaction with the player
	UStaticMeshComponent *m_VisualComponentPtr = nullptr;

	bool m_IsMain = false;

	State m_State = State::Idle;

	float m_SnappedFloorGridCellHalfSizeInUu = -1.0f; // only SnapToFloorGrid() makes this valid
};


bool ALevelDoorKeyElement::IsMain () const noexcept
{
	return m_IsMain;
}

bool ALevelDoorKeyElement::IsInPressedState () const noexcept
{
	return (m_State == State::Pressed);
}

template<>
inline UMaterialInterface*& ALevelDoorKeyElement::StateMaterialPtr<ALevelDoorKeyElement::State::Idle> () noexcept
{
	return m_IdleStateMaterialPtr;
}

template<>
inline UMaterialInterface*& ALevelDoorKeyElement::StateMaterialPtr<ALevelDoorKeyElement::State::Pressed> () noexcept
{
	return m_PressedStateMaterialPtr;
}

template<>
inline UMaterialInterface*& ALevelDoorKeyElement::StateMaterialPtr<ALevelDoorKeyElement::State::EntireKeyActivated> (
	) noexcept
{
	return m_EntireKeyActivatedStateMaterialPtr;
}
