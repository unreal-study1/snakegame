#pragma once

#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"

#include "Interactable.h"
#include "SnakeElementBase.generated.h"

struct FBox;
class UPrimitiveComponent;
class UStaticMeshComponent;

class ASnakeBase;

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	inline static ECollisionChannel GetBlockingCollisionChannel () noexcept;

public:
	explicit ASnakeElementBase ();
	~ASnakeElementBase () override = default;

	ASnakeBase* GetOwnerSnakePtr () const;

	UFUNCTION(BlueprintNativeEvent)
	void MarkAsHead (); // will be redefined in a derived blueprint class
	void MarkAsHead_Implementation ();

	UFUNCTION(BlueprintCallable)
	inline bool IsHead () const noexcept;

	bool DoesBoxIntersectElementBoundingBox (const FBox &BoxInUu) const;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (DisplayName = "Mesh Component", ToolTip = ""))
	UStaticMeshComponent *m_MeshComponentPtr = nullptr;

protected:
	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

	void StartInteraction (AActor *InteractorPtr) override;

private:
	UFUNCTION() // delegate binding requires it
	void HandleBeginOverlap (
		UPrimitiveComponent *OverlappedComponentPtrOfThis,
		AActor *OtherActorPtr, UPrimitiveComponent *OverlappedComponentPtrOfOther,
		int32 OtherBodyIndex,
		bool IsFromSweep, const FHitResult &SweepResult);
	UFUNCTION()
	void HandleEndOverlap (
		UPrimitiveComponent *OverlappedComponentPtrOfThis,
		AActor *OtherActorPtr, UPrimitiveComponent *OverlappedComponentPtrOfOther,
		int32 OtherBodyIndex);

	UFUNCTION() // delegate binding requires it
	void HandleHit (
		UPrimitiveComponent *HitComponentPtrOfThis,
		AActor *OtherActorPtr, UPrimitiveComponent *HitComponentPtrOfAnother,
		FVector NormalImpulse, const FHitResult &HitResult);

private:
	bool m_IsHead = false;
};


ECollisionChannel ASnakeElementBase::GetBlockingCollisionChannel () noexcept
{
	return ECollisionChannel::ECC_WorldStatic;
}

bool ASnakeElementBase::IsHead () const noexcept
{
	return m_IsHead;
}
