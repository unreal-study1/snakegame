#include "FloorGridFragment.h"

#include "Components/StaticMeshComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"
#include "Math/IntPoint.h"
#include "Misc/AssertionMacros.h"
#include "Templates/Casts.h"

const TCHAR* AFloorGridFragment::m_XyGridMaterialParamNameForCellSizeInUu = TEXT("Cell Size (uu)");
const TCHAR* AFloorGridFragment::m_XyGridMaterialParamNameForLineThicknessInUu = TEXT("Line Thickness (uu)");
const TCHAR* AFloorGridFragment::m_XyGridMaterialParamNameForRelativeOffsetFromWorldOriginInPercent =
	TEXT("Relative Offset (%) from World Origin");

AFloorGridFragment::AFloorGridFragment ()
{
	this->PrimaryActorTick.bCanEverTick = false;

	m_MeshComponentPtr = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	check(::IsValid(m_MeshComponentPtr));

	const auto XyPlanePtr =
		::Cast<UStaticMesh>(::StaticLoadObject(UStaticMesh::StaticClass(), nullptr, TEXT("/Engine/BasicShapes/Plane")));
	check(::IsValid(XyPlanePtr));

	m_MeshSizesInUu = XyPlanePtr->GetBoundingBox().GetSize();

	static_cast<void>(m_MeshComponentPtr->SetStaticMesh(XyPlanePtr));

	m_MeshComponentPtr->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	m_MeshComponentPtr->SetGenerateOverlapEvents(false);

	m_MeshComponentPtr->SetCastShadow(false);

	this->SetRootComponent(m_MeshComponentPtr);
}

void AFloorGridFragment::SetLocationZ (const float ZinUu)
{
	FVector Location = this->GetActorLocation();
	Location.Z = ZinUu;
	static_cast<void>(this->SetActorLocation(Location));
}

void AFloorGridFragment::SetXyGridMaterial (UMaterialInstance *MaterialPtr)
{
	check(::IsValid(m_MeshComponentPtr));

	m_MeshComponentPtr->SetMaterial(0, MaterialPtr);

	UpdateXyLocationAndScale();
}

const UMaterialInstance* AFloorGridFragment::GetXyGridMaterial () const
{
	check(::IsValid(m_MeshComponentPtr));

	return ::Cast<UMaterialInstance>(m_MeshComponentPtr->GetMaterial(0));
}

float AFloorGridFragment::GetGridCellSizeInUu () const
{
	return GetFloatParamOfXyGridMaterial(AFloorGridFragment::m_XyGridMaterialParamNameForCellSizeInUu);
}

float AFloorGridFragment::GetLineThicknessInUu () const
{
	return GetFloatParamOfXyGridMaterial(AFloorGridFragment::m_XyGridMaterialParamNameForLineThicknessInUu);
}

FVector AFloorGridFragment::ComputeCellCenterInUu (
	const int32 CellIndexAlongWorldX, const int32 CellIndexAlongWorldY) const
{
	const float CellSizeInUu = GetGridCellSizeInUu();

	auto convertCellIndexToOffsetInUu = [CellSizeInUu](const int32 CellIndex)
	{
		return ((float(CellIndex) + 0.5f) * CellSizeInUu);
	};

	const FVector BottomLeftCellLocationInUu = ComputeBottomLeftCellLocationInUu();

	FVector CellCenterInUu(
		BottomLeftCellLocationInUu.X + convertCellIndexToOffsetInUu(CellIndexAlongWorldX),
		BottomLeftCellLocationInUu.Y + convertCellIndexToOffsetInUu(CellIndexAlongWorldY),
		this->GetActorLocation().Z);
	return CellCenterInUu;
}

FIntPoint AFloorGridFragment::FindIndexOfCellContainingPoint (const FVector2D &PointInUu) const
{
	const FVector2D BottomLeftCellLocationInUu(ComputeBottomLeftCellLocationInUu());
	const float CellSizeInUu = GetGridCellSizeInUu();
	const FVector2D ApproximateCellIndex = (PointInUu - BottomLeftCellLocationInUu) / CellSizeInUu;

	return FIntPoint(FMath::TruncToInt(ApproximateCellIndex.X), FMath::TruncToInt(ApproximateCellIndex.Y));
}

FIntPoint AFloorGridFragment::FindIndexOfCellContainingPoint (const FVector &PointInUu) const
{
	return FindIndexOfCellContainingPoint(FVector2D(PointInUu));
}

void AFloorGridFragment::BeginPlay ()
{
	Super::BeginPlay();

	// make sure that we have correct x-y location and scale in shipping builds:
	UpdateXyLocationAndScale();
}

#if WITH_EDITOR
void AFloorGridFragment::PostEditChangeProperty (FPropertyChangedEvent &Event)
{
	Super::PostEditChangeProperty(Event);

	if (Event.Property == nullptr)
		return;

	const FName PropertyName = Event.Property->GetFName();
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(AFloorGridFragment, m_NumCellsAlongWorldX)) ||
		(PropertyName == GET_MEMBER_NAME_CHECKED(AFloorGridFragment, m_NumCellsAlongWorldY)) ||
		(PropertyName == GET_MEMBER_NAME_CHECKED(AFloorGridFragment, m_OffsetFromWorldOriginXyInCells)))
	{
		UpdateXyLocationAndScale();
	}
}
#endif

float AFloorGridFragment::FixOffsetFromWorldOriginInCells (const float RawOffsetInCells)
{
	// members of 'm_OffsetFromWorldOriginXyInCells' must be a multiple of 0.5;
	// I didn't find how to do it through UPROPERTY() ("meta = (Multiple = 0.5)" doesn't work)
	return FMath::GridSnap(RawOffsetInCells, 0.5f);
}

float AFloorGridFragment::ComputeGridOffsetFromWorldOriginInUu (
	const float RelativeGridOffsetFromWorldOriginInPercent, const float GridCellSizeInUu)
{
	return ((1.0f - (RelativeGridOffsetFromWorldOriginInPercent / 100.0f)) * GridCellSizeInUu * 0.5f);
}

void AFloorGridFragment::UpdateXyLocationAndScale ()
{
	check(::IsValid(m_MeshComponentPtr));

	const float CellSizeInUu = GetGridCellSizeInUu();
	const float LineThicknessInUu = GetLineThicknessInUu();
	const float RelativeGridOffsetFromWorldOriginInPercent =
		GetRelativeGridOffsetFromWorldOriginInPercent();

	if ((CellSizeInUu < 0.0f) || (LineThicknessInUu < 0.0f) || (RelativeGridOffsetFromWorldOriginInPercent < 0.0f))
		return;

	const float GridOffsetFromWorldOriginInUu =
		AFloorGridFragment::ComputeGridOffsetFromWorldOriginInUu(
			RelativeGridOffsetFromWorldOriginInPercent, CellSizeInUu);

	auto convertOffsetFromNumberOfCellsToUu = [CellSizeInUu, GridOffsetFromWorldOriginInUu](const float NumCellsAsFloat)
	{
		const float CorrectNumCellsAsFloat = AFloorGridFragment::FixOffsetFromWorldOriginInCells(NumCellsAsFloat);
		return ((CorrectNumCellsAsFloat * CellSizeInUu) + GridOffsetFromWorldOriginInUu);
	};

	FVector LocationInUu = this->GetActorLocation();
	LocationInUu.X = convertOffsetFromNumberOfCellsToUu(m_OffsetFromWorldOriginXyInCells.X);
	LocationInUu.Y = convertOffsetFromNumberOfCellsToUu(m_OffsetFromWorldOriginXyInCells.Y);
	static_cast<void>(this->SetActorLocation(LocationInUu));

	FVector Scale3D(0, 0, 1);
	Scale3D.X = (m_NumCellsAlongWorldX * CellSizeInUu + LineThicknessInUu) / m_MeshSizesInUu.X;
	Scale3D.Y = (m_NumCellsAlongWorldY * CellSizeInUu + LineThicknessInUu) / m_MeshSizesInUu.Y;
	m_MeshComponentPtr->SetWorldScale3D(Scale3D);
}

float AFloorGridFragment::GetFloatParamOfXyGridMaterial (const TCHAR *ParamName) const
{
	constexpr float InvalidParamValue = -1.0f;

	const UMaterialInstance *XyGridMaterialPtr = GetXyGridMaterial();

	if (XyGridMaterialPtr == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("XyGridMaterial was not set"));
		return InvalidParamValue;
	}

	float ParamValue = 0.0f;

	if (!XyGridMaterialPtr->GetScalarParameterValue(ParamName, ParamValue))
	{
		UE_LOG(LogTemp, Warning, TEXT("XyGridMaterial does not have '%s' parameter"), ParamName);
		return InvalidParamValue;
	}

	return ParamValue;
}

float AFloorGridFragment::GetRelativeGridOffsetFromWorldOriginInPercent () const
{
	const auto ParamName = AFloorGridFragment::m_XyGridMaterialParamNameForRelativeOffsetFromWorldOriginInPercent;
	return GetFloatParamOfXyGridMaterial(ParamName);
}

FVector AFloorGridFragment::ComputeBottomLeftCellLocationInUu () const
{
	FVector GridFragmentOriginInUu;
	FVector GridFragmentHalfSizesInUu;
	constexpr bool AreOnlyCollidingComponentsDefineBounds = false;
	this->GetActorBounds(AreOnlyCollidingComponentsDefineBounds, GridFragmentOriginInUu, GridFragmentHalfSizesInUu);

	const float HalfLineThicknessInUu = GetLineThicknessInUu() * 0.5f;
	const FVector2D ResultXyLocationInUu =
		FVector2D(GridFragmentOriginInUu) - FVector2D(GridFragmentHalfSizesInUu) + FVector2D(HalfLineThicknessInUu);
	return FVector(ResultXyLocationInUu, GridFragmentOriginInUu.Z);
}
