#include "LevelDoorKey.h"

#include "LevelDoorKeyElement.h"

#include "Misc/AssertionMacros.h"

ALevelDoorKey::ALevelDoorKey ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame
}

void ALevelDoorKey::AddElement (ALevelDoorKeyElement *ElementPtr)
{
	if (::IsValid(ElementPtr) && !m_ElementsPtrs.Contains(ElementPtr))
	{
		static_cast<void>(m_ElementsPtrs.Add(ElementPtr));

		ElementPtr->SetOwner(this);
		ElementPtr->SetActorHiddenInGame(!IsVisible());
	}
}

void ALevelDoorKey::SetVisible (const bool IsVisible)
{
	if (IsVisible == this->IsVisible())
		return;

	this->SetActorHiddenInGame(!IsVisible);

	for (ALevelDoorKeyElement *ElementPtr : m_ElementsPtrs)
	{
		check(::IsValid(ElementPtr));

		ElementPtr->SetActorHiddenInGame(!IsVisible);
	}
}

bool ALevelDoorKey::IsVisible () const
{
	return !this->IsHidden();
}

void ALevelDoorKey::SnapToFloorGrid (const AFloorGridFragment *GridPtr)
{
	for (ALevelDoorKeyElement *ElementPtr : m_ElementsPtrs)
	{
		check(::IsValid(ElementPtr));

		ElementPtr->SnapToFloorGrid(GridPtr);
	}
}

void ALevelDoorKey::ActivateKeyIfAllElementsArePressed ()
{
	if (m_ElementsPtrs.Num() < 1)
		return;

	for (const ALevelDoorKeyElement *ElementPtr : m_ElementsPtrs)
	{
		check(::IsValid(ElementPtr));

		if (!ElementPtr->IsInPressedState())
			return;
	}

	for (ALevelDoorKeyElement *ElementPtr : m_ElementsPtrs)
		ElementPtr->EnterEntireKeyActivatedState();

	m_KeyActivationEvent.Broadcast(this);
}

void ALevelDoorKey::DeactivateKey ()
{
	for (ALevelDoorKeyElement *ElementPtr : m_ElementsPtrs)
	{
		check(::IsValid(ElementPtr));

		ElementPtr->LeaveEntireKeyActivatedState();
	}
}

void ALevelDoorKey::BeginPlay ()
{
	Super::BeginPlay();
}
