#pragma once

#include "GameFramework/GameModeBase.h"
#include "UObject/ObjectMacros.h"

#include "SnakeGameGameModeBase.generated.h"

UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
