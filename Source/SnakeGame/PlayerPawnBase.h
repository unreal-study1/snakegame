#pragma once

#include "GameFramework/Pawn.h"
#include "Templates/SubclassOf.h"
#include "UObject/ObjectMacros.h"

#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class UInputComponent;
class USpringArmComponent;

class ASnakeBase;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	APlayerPawnBase ();
	~APlayerPawnBase () override = default;

	UFUNCTION(BlueprintCallable)
	void SetCameraPitchInDeg (const float Pitch);
	UFUNCTION(BlueprintCallable, meta = (ReturnDisplayName = "Pitch"))
	inline float GetCameraPitchInDeg () const;

	UFUNCTION(BlueprintCallable)
	void Freeze ();
	UFUNCTION(BlueprintCallable)
	void Unfreeze ();

	// note: it is called to bind functionality to input
	void SetupPlayerInputComponent (UInputComponent *PlayerInputComponentPtr) override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (DisplayName = "Spring Arm", ToolTip = ""))
	USpringArmComponent *m_SpringArmPtr = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (DisplayName = "Camera", ToolTip = ""))
	UCameraComponent *m_CameraPtr = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (DisplayName = "Snake Actor", ToolTip = ""))
	ASnakeBase *m_SnakeActorPtr = nullptr;

	UPROPERTY(EditDefaultsOnly, meta = (DisplayName = "Snake Actor Class", ToolTip = ""))
	TSubclassOf<ASnakeBase> m_SnakeActorClass;

protected:
	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

private:
	void CreateSnakeActor ();

	void UpdateRotation ();

	void HandlePlayerVerticalInput (const float InputValue);
	void HandlePlayerHorizontalInput (const float InputValue);

private:
	float m_CameraPitchInDeg = -90.0f;
};


float APlayerPawnBase::GetCameraPitchInDeg () const
{
	return m_CameraPitchInDeg;
}
