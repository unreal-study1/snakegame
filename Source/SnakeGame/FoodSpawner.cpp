#include "FoodSpawner.h"

#include "FloorGridFragment.h"
#include "Food.h"

#include "Components/StaticMeshComponent.h"
#include "Curves/CurveVector.h"
#include "Engine/Public/TimerManager.h"
#include "Misc/AssertionMacros.h"

#include <limits>

AFoodSpawner::AFoodSpawner ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame

	m_SpawnerMeshComponentPtr = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	check(::IsValid(m_SpawnerMeshComponentPtr));

	this->SetRootComponent(m_SpawnerMeshComponentPtr);

	m_SpawnerMeshComponentPtr->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	m_SpawnerMeshComponentPtr->SetCollisionResponseToChannels(ECollisionResponse::ECR_Block);

	m_SpawnedFoodPlacingTimeline.SetTimelineLengthMode(ETimelineLengthMode::TL_LastKeyFrame);
	m_SpawnedFoodPlacingTimeline.SetLooping(false);

	FOnTimelineEvent SpawnedFoodPlacingTimelineFinishDelegate;
	SpawnedFoodPlacingTimelineFinishDelegate.BindUFunction(
		this, GET_FUNCTION_NAME_CHECKED(AFoodSpawner, TrackSpawnedFoodFlightFinish));
	m_SpawnedFoodPlacingTimeline.SetTimelineFinishedFunc(SpawnedFoodPlacingTimelineFinishDelegate);
}

void AFoodSpawner::StartSpawningFood ()
{
	if (!this->HasActorBegunPlay())
		return;

	if (m_WasFoodSpawningStarted)
		return;

	m_WasFoodSpawningStarted = true;

	SpawnFoodAfterDelay();
}

void AFoodSpawner::StopSpawningFood ()
{
	if (!m_WasFoodSpawningStarted)
		return;

	if (m_FoodSpawningTimerHandle.IsValid() && ::IsValid(this->GetWorld()))
	{
		this->GetWorldTimerManager().ClearTimer(m_FoodSpawningTimerHandle);

		m_WasFoodSpawningStarted = false;
	}
}

void AFoodSpawner::AddAreaAvailableForFood (const AFloorGridFragment *AreaPtr)
{
	if (::IsValid(AreaPtr))
		static_cast<void>(m_PtrsToAreasAvailableForFood.AddUnique(AreaPtr));
}

void AFoodSpawner::AddAreaUnavailableForFood (const AActor *AreaPtr)
{
	if (::IsValid(AreaPtr))
	{
		const FBox AreaBoundingBoxInUu = AFoodSpawner::GetActorBoundingBoxInUu(*AreaPtr);

		static_cast<void>(
			m_RectsInUuOfAreasUnavailableForFood.Emplace(
				FVector2D(AreaBoundingBoxInUu.Min), FVector2D(AreaBoundingBoxInUu.Max)));
	}
}

void AFoodSpawner::Tick (float DeltaTimeInS)
{
	Super::Tick(DeltaTimeInS);

	if (::IsValid(m_SpawnedFoodPtr))
		m_SpawnedFoodPlacingTimeline.TickTimeline(DeltaTimeInS);
}

void AFoodSpawner::BeginPlay ()
{
	Super::BeginPlay();

	AddAreaUnavailableForFood(this);

	m_SpawnedFoodPlacingCurvePtr = ::NewObject<UCurveVector>(this, TEXT("SpawnedFoodPlacingCurve"));
	// 1. this->CreateDefaultSubobject() doesn't seem to actually work for UCurveVector
	// 2. don't move this line into AFoodSpawner's constructor as 'm_SpawnedFoodPlacingCurvePtr' will become nullptr
	//    in BeginPlay(); marking 'm_SpawnedFoodPlacingCurvePtr' with "UPROPERTY()" doesn't help

	FOnTimelineVector FoodLocationUpdatingDelegate;
	FoodLocationUpdatingDelegate.BindUFunction(this, GET_FUNCTION_NAME_CHECKED(AFoodSpawner, SetLocationOfSpawnedFood));
	m_SpawnedFoodPlacingTimeline.AddInterpVector(m_SpawnedFoodPlacingCurvePtr, FoodLocationUpdatingDelegate);
}

void AFoodSpawner::EndPlay (const EEndPlayReason::Type EndPlayReason)
{
	StopSpawningFood();

	Super::EndPlay(EndPlayReason);
}

float AFoodSpawner::ComputeHalfSizeOfBoxAlongZ (const FBox &Box) noexcept
{
	return (Box.GetSize().Z * 0.5f);
}

FBox AFoodSpawner::GetActorBoundingBoxInUu (const AActor &Actor)
{
	const bool AreNoncollidingComponentsIncludedToo = true;
	return Actor.GetComponentsBoundingBox(AreNoncollidingComponentsIncludedToo);
}

uint32 AFoodSpawner::GenerateRandomNumber (const uint32 ExclusiveMax)
{
	check(ExclusiveMax <= uint32(std::numeric_limits<int32>::max() - int32(1)));

	if (ExclusiveMax == uint32(0))
		return ExclusiveMax;

	const auto RawResult =
		FMath::RandRange(int32(0), int32(ExclusiveMax) - int32(1)); // FMath::RandRange() includes both borders
	return uint32(RawResult);
}

int32 AFoodSpawner::ChooseRandomIndexInArrayBasedOnItemsMagnitudes (const TArray<uint32> &ItemsMagnitudes)
{
	if (ItemsMagnitudes.Num() < 1)
		return ItemsMagnitudes.Num();

	uint32 SumMagnitudes = 0;

	for (const uint32 Magnitude : ItemsMagnitudes)
		SumMagnitudes += Magnitude;

	const uint32 SumMagnitudesPriorToAndIncludedTargetOne = AFoodSpawner::GenerateRandomNumber(SumMagnitudes);

	int32 TargetItemIndex = 0;
	uint32 SumMagnitudesPriorToTargetOne = 0;

	for (TargetItemIndex = 0; TargetItemIndex < ItemsMagnitudes.Num(); ++TargetItemIndex)
	{
		const uint32 NextSumMagnitudes = SumMagnitudesPriorToTargetOne + ItemsMagnitudes[TargetItemIndex];

		if (FMath::IsWithin(SumMagnitudesPriorToAndIncludedTargetOne, SumMagnitudesPriorToTargetOne, NextSumMagnitudes))
			return TargetItemIndex;

		SumMagnitudesPriorToTargetOne = NextSumMagnitudes;
	}

	return (ItemsMagnitudes.Num() - 1);
}

FVector AFoodSpawner::ComputeTopCenterInUu () const
{
	const FBox BoundingBoxInUu = AFoodSpawner::GetActorBoundingBoxInUu(*this);
	return (BoundingBoxInUu.GetCenter() + FVector(0, 0, AFoodSpawner::ComputeHalfSizeOfBoxAlongZ(BoundingBoxInUu)));
}

void AFoodSpawner::SpawnFoodAfterDelay ()
{
	if (!::IsValid(this->GetWorld()))
		return;

	FTimerManager &TimerManager = this->GetWorldTimerManager();

	if (TimerManager.GetTimerRemaining(m_FoodSpawningTimerHandle) > 0.0f)
		return; // we've already started

	const float TimerIntervalInS = AFoodSpawner::ConvertMsToS(m_FoodSpawningDelayInMs);
	const bool IsLoopingTimer = false;
	TimerManager.SetTimer(
		m_FoodSpawningTimerHandle, this, &AFoodSpawner::SpawnFoodNow, TimerIntervalInS, IsLoopingTimer);
}

void AFoodSpawner::SpawnFoodNow ()
{
	UWorld *WorldPtr = this->GetWorld();
	check(::IsValid(WorldPtr));

	const FVector SpawnerTopCenterInUu = ComputeTopCenterInUu();

	FActorSpawnParameters FoodActorSpawnParams;
	FoodActorSpawnParams.Owner = this;
	FoodActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	m_SpawnedFoodPtr = WorldPtr->SpawnActor<AFood>(m_FoodClass, FTransform(SpawnerTopCenterInUu), FoodActorSpawnParams);
	check(::IsValid(m_SpawnedFoodPtr));

	// when this food is destroyed, notify clients and start spawning the next one:
	FScriptDelegate FoodDestructionDelegate1, FoodDestructionDelegate2;
	FoodDestructionDelegate1.BindUFunction(this, GET_FUNCTION_NAME_CHECKED(AFoodSpawner, SpawnedFoodDestroyed));
	FoodDestructionDelegate2.BindUFunction(this, GET_FUNCTION_NAME_CHECKED(AFoodSpawner, SpawnFoodAfterDelay));
	m_SpawnedFoodPtr->OnDestroyed.Add(FoodDestructionDelegate1);
	m_SpawnedFoodPtr->OnDestroyed.Add(FoodDestructionDelegate2);

	const FVector InitialLocationForFoodInUu =
		SpawnerTopCenterInUu +
		FVector(0, 0, AFoodSpawner::ComputeHalfSizeOfActorBoundingBoxAlongZinUu(*m_SpawnedFoodPtr)) +
		FVector(0, 0, 1); // "+eps" to prevent collision with 'this'
	const FVector FinalLocationForFoodInUu = MakeFinalLocationForNewFoodInUu(*m_SpawnedFoodPtr);

	m_SpawnedFoodPtr->SetActorEnableCollision(false); // ignore everything while flying

	/*trigger event*/ SpawnedFoodFlightStarted(FinalLocationForFoodInUu, m_SpawnedFoodPtr);

	InitSpawnedFoodPlacingCurve(InitialLocationForFoodInUu, FinalLocationForFoodInUu);

	m_SpawnedFoodPlacingTimeline.PlayFromStart();
}

FVector AFoodSpawner::MakeFinalLocationForNewFoodInUu (const AFood &Food)
{
	m_NumCellsInAreasAvailableForFoodOnSpawning.Empty(m_PtrsToAreasAvailableForFood.Num());
	for (const AFloorGridFragment *AreaPtr : m_PtrsToAreasAvailableForFood)
	{
		if (::IsValid(AreaPtr)) // regardless of the same check in AddAreaAvailableForFood()
		{
			m_NumCellsInAreasAvailableForFoodOnSpawning.Add(
				AreaPtr->m_NumCellsAlongWorldX * AreaPtr->m_NumCellsAlongWorldY);
		}
	}

	FVector LocationOfFoodBottomInUu = ComputeTopCenterInUu();

	if (m_NumCellsInAreasAvailableForFoodOnSpawning.Num() > 0)
	{
		const int NumAttempts = 5;
		for (int AttemptIndex = 0; AttemptIndex < NumAttempts; ++AttemptIndex)
		{
			int32 TargetAreaIndex =
				AFoodSpawner::ChooseRandomIndexInArrayBasedOnItemsMagnitudes(
					m_NumCellsInAreasAvailableForFoodOnSpawning);
			check(TargetAreaIndex < m_NumCellsInAreasAvailableForFoodOnSpawning.Num());
			
			int NumInvalidAreasBeforeTargetOne = 0;

			for (int32 i = 0; i <= TargetAreaIndex; ++i)
			{
				if (!::IsValid(m_PtrsToAreasAvailableForFood[i]))
					++NumInvalidAreasBeforeTargetOne;
			}

			TargetAreaIndex += NumInvalidAreasBeforeTargetOne;
			check(::IsValid(m_PtrsToAreasAvailableForFood[TargetAreaIndex]));

			const AFloorGridFragment &TargetArea = *m_PtrsToAreasAvailableForFood[TargetAreaIndex];
			const uint32 TargetCellIndexX = AFoodSpawner::GenerateRandomNumber(TargetArea.m_NumCellsAlongWorldX);
			const uint32 TargetCellIndexY = AFoodSpawner::GenerateRandomNumber(TargetArea.m_NumCellsAlongWorldY);

			const FVector LocationCandidateOfFoodBottomInUu =
				TargetArea.ComputeCellCenterInUu(TargetCellIndexX, TargetCellIndexY);
			if (!IsLocationInsideAreasUnavailableForFood(LocationCandidateOfFoodBottomInUu))
			{
				LocationOfFoodBottomInUu = LocationCandidateOfFoodBottomInUu;
				break;
			}
		}
	}

	return (LocationOfFoodBottomInUu + FVector(0, 0, AFoodSpawner::ComputeHalfSizeOfActorBoundingBoxAlongZinUu(Food)));
}

FVector AFoodSpawner::ComputeHighestPointOfFoodFlyingArc (
	const FVector &ArcStartPoint, const FVector &ArcEndPoint) const noexcept
{
	const FVector ArcDirectionVector = ArcEndPoint - ArcStartPoint;
	const FVector ArcDirectionOrthogonalVector = FVector::CrossProduct(ArcDirectionVector, FVector::UpVector);

	FVector ArcHeightDirectionVector =
		FVector::CrossProduct(ArcDirectionVector, ArcDirectionOrthogonalVector).GetUnsafeNormal();
	if (ArcHeightDirectionVector.Z < 0.0f)
		ArcHeightDirectionVector *= (-1.0f);

	const FVector PointBetweenArcEnds =
		FMath::Lerp(ArcStartPoint, ArcEndPoint, m_RelativePositionOfHighestPointOfSpawnedFoodFlyingArc);
	const FVector DirectionToResultPoint =
		ArcHeightDirectionVector * (ArcDirectionVector.Size() * m_SpawnedFoodFlyingArcHeightToLengthRatio);

	return (PointBetweenArcEnds + DirectionToResultPoint);
}

bool AFoodSpawner::IsLocationInsideAreasUnavailableForFood (const FVector &LocationInUu) const
{
	const FVector2D Location2DInUu(LocationInUu);

	for (const FBox2D &RectInUu : m_RectsInUuOfAreasUnavailableForFood)
	{
		if (RectInUu.IsInside(Location2DInUu))
			return true;
	}

	return false;
}

void AFoodSpawner::InitSpawnedFoodPlacingCurve (
	const FVector &InitialLocationForFoodInUu, const FVector &FinalLocationForFoodInUu)
{
	check(m_SpawnedFoodFlyingVelocityInUuPerS >= 1);
	check(::IsValid(m_SpawnedFoodPlacingCurvePtr));

	const float FoodSpawningDurationInS =
		FVector::Dist(InitialLocationForFoodInUu, FinalLocationForFoodInUu) /
		float(m_SpawnedFoodFlyingVelocityInUuPerS);

	const FVector HighestLocationForFoodInUu =
		ComputeHighestPointOfFoodFlyingArc(InitialLocationForFoodInUu, FinalLocationForFoodInUu);
	const float HighestPointTimeInS = FoodSpawningDurationInS * m_RelativePositionOfHighestPointOfSpawnedFoodFlyingArc;

	for (int32 VectorComponentIndex = 0; VectorComponentIndex < 3; ++VectorComponentIndex)
	{
		FRichCurve &VectorComponentCurve = m_SpawnedFoodPlacingCurvePtr->FloatCurves[VectorComponentIndex];

		VectorComponentCurve.Reset();

		const FKeyHandle StartPointHandle =
			VectorComponentCurve.AddKey(0.0f, InitialLocationForFoodInUu[VectorComponentIndex]);
		const FKeyHandle HighestPointHandle =
			VectorComponentCurve.AddKey(HighestPointTimeInS, HighestLocationForFoodInUu[VectorComponentIndex]);
		const FKeyHandle EndPointHandle =
			VectorComponentCurve.AddKey(FoodSpawningDurationInS, FinalLocationForFoodInUu[VectorComponentIndex]);

		for (const FKeyHandle PointHandle : {StartPointHandle, HighestPointHandle, EndPointHandle})
			VectorComponentCurve.SetKeyInterpMode(PointHandle, RCIM_Cubic);
	}
}

void AFoodSpawner::SetLocationOfSpawnedFood (const FVector &LocationInUu)
{
	if (::IsValid(m_SpawnedFoodPtr))
		m_SpawnedFoodPtr->SetActorLocation(LocationInUu);
}

void AFoodSpawner::TrackSpawnedFoodFlightFinish ()
{
	if (::IsValid(m_SpawnedFoodPtr))
		m_SpawnedFoodPtr->SetActorEnableCollision(true); // see paired call in SpawnFoodNow()

	SpawnedFoodFlightFinished();
}
