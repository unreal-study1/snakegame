#include "SnakeElementBase.h"

#include "SnakeBase.h"

#include "Components/StaticMeshComponent.h"
#include "Misc/AssertionMacros.h"
#include "Templates/Casts.h"

ASnakeElementBase::ASnakeElementBase ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame

	m_MeshComponentPtr = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	check(::IsValid(m_MeshComponentPtr));

	m_MeshComponentPtr->SetCollisionObjectType(ECollisionChannel::ECC_Destructible);
	m_MeshComponentPtr->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	this->SetRootComponent(m_MeshComponentPtr);
}

ASnakeBase* ASnakeElementBase::GetOwnerSnakePtr () const
{
	return ::Cast<ASnakeBase>(this->GetOwner());
}

void ASnakeElementBase::MarkAsHead_Implementation ()
{
	check(::IsValid(m_MeshComponentPtr));

	m_IsHead = true;

	m_MeshComponentPtr->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
}

bool ASnakeElementBase::DoesBoxIntersectElementBoundingBox (const FBox &BoxInUu) const
{
	constexpr bool AreOnlyCollidingComponentsDefineBounds = false;
	const FBox BoundingBoxInUu = this->GetComponentsBoundingBox(!AreOnlyCollidingComponentsDefineBounds);

	return BoundingBoxInUu.Intersect(BoxInUu);
}

void ASnakeElementBase::BeginPlay ()
{
	check(::IsValid(m_MeshComponentPtr));

	Super::BeginPlay();

	// these don't work when called from ASnakeElementBase's constructor:

	FCollisionResponseContainer CollisionResponseContainer(ECollisionResponse::ECR_Overlap);
	static_cast<void>(
		CollisionResponseContainer.SetResponse(
			ASnakeElementBase::GetBlockingCollisionChannel(), ECollisionResponse::ECR_Block));
	m_MeshComponentPtr->SetCollisionResponseToChannels(CollisionResponseContainer);

	m_MeshComponentPtr->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
	m_MeshComponentPtr->OnComponentEndOverlap.AddDynamic(this, &ASnakeElementBase::HandleEndOverlap);
	m_MeshComponentPtr->OnComponentHit.AddDynamic(this, &ASnakeElementBase::HandleHit);
}

void ASnakeElementBase::StartInteraction (AActor *InteractorPtr)
{
	const auto SnakeElementPtr = ::Cast<ASnakeElementBase>(InteractorPtr);
	if (::IsValid(SnakeElementPtr) && (SnakeElementPtr != this))
	{
		// without this check the whole snake will "collapse" to single element on food eating
		if (SnakeElementPtr->IsHead())
		{
			ASnakeBase *SnakePtr = SnakeElementPtr->GetOwnerSnakePtr();
			if (::IsValid(SnakePtr))
				SnakePtr->DestroyAndEndGame();
		}
	}
}

void ASnakeElementBase::HandleBeginOverlap (
	UPrimitiveComponent* /*OverlappedComponentPtrOfThis*/,
	AActor *OtherActorPtr, UPrimitiveComponent* /*OverlappedComponentPtrOfOther*/,
	int32 /*OtherBodyIndex*/,
	bool /*IsFromSweep*/, const FHitResult& /*SweepResult*/)
{
	ASnakeBase *OwnerSnakePtr = GetOwnerSnakePtr();
	if (::IsValid(OwnerSnakePtr))
		OwnerSnakePtr->HandleElementOverlapBegin(this, OtherActorPtr);
}

void ASnakeElementBase::HandleEndOverlap (
	UPrimitiveComponent* /*OverlappedComponentPtrOfThis*/,
	AActor *OtherActorPtr, UPrimitiveComponent* /*OverlappedComponentPtrOfOther*/,
	int32 /*OtherBodyIndex*/)
{
	ASnakeBase *OwnerSnakePtr = GetOwnerSnakePtr();
	if (::IsValid(OwnerSnakePtr))
		OwnerSnakePtr->HandleElementOverlapEnd(this, OtherActorPtr);
}

void ASnakeElementBase::HandleHit (
	UPrimitiveComponent* /*HitComponentPtrOfThis*/,
	AActor *OtherActorPtr, UPrimitiveComponent* /*HitComponentPtrOfAnother*/,
	FVector /*NormalImpulse*/, const FHitResult& /*HitResult*/)
{
	ASnakeBase *OwnerSnakePtr = GetOwnerSnakePtr();
	if (::IsValid(OwnerSnakePtr))
		OwnerSnakePtr->HandleElementHit(this, OtherActorPtr);
}
