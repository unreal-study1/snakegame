#pragma once

#include "Containers/Array.h"
#include "Delegates/DelegateCombinations.h"
#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"

#include "LevelDoorKey.generated.h"

class AFloorGridFragment;
class ALevelDoorKeyElement;

UCLASS()
class SNAKEGAME_API ALevelDoorKey : public AActor
{
	GENERATED_BODY()
	
public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKeyActivationEventSignature, ALevelDoorKey*, LevelDoorKey);

public:
	ALevelDoorKey ();
	~ALevelDoorKey () override = default;

	UFUNCTION(BlueprintCallable, meta = (ToolTip = "It does nothing if 'Element' is empty or was added before"))
	void AddElement (
		UPARAM(meta = (DisplayName = "Element"))
		ALevelDoorKeyElement *ElementPtr);

	UFUNCTION(BlueprintCallable, meta = (ToolTip = "It is better than SetActorHiddenInGame() because it also changes visibility of the elements"))
	void SetVisible (const bool IsVisible);
	UFUNCTION(BlueprintCallable)
	bool IsVisible () const;

	UFUNCTION(BlueprintCallable, meta = (ToolTip = "It does nothing if 'Floor Grid' is empty"))
	void SnapToFloorGrid (
		UPARAM(meta = (DisplayName = "Floor Grid"))
		const AFloorGridFragment *GridPtr);

	void ActivateKeyIfAllElementsArePressed ();

	UFUNCTION(BlueprintCallable)
	void DeactivateKey ();

public:
	// called when ActivateKeyIfAllElementsArePressed() succeeds
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "On Key Activated"))
	FKeyActivationEventSignature m_KeyActivationEvent;

protected:
	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

private:
	TArray<ALevelDoorKeyElement*> m_ElementsPtrs;
};
