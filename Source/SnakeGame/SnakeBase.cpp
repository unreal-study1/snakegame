#include "SnakeBase.h"

#include "Interactable.h"
#include "SnakeElementBase.h"

#include "Engine/Engine.h"
#include "Engine/Public/TimerManager.h"
#include "Misc/AssertionMacros.h"
#include "Templates/Casts.h"

ASnakeBase::ASnakeBase ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame

	this->SetActorEnableCollision(true);
}

ASnakeElementBase* ASnakeBase::HeadElementPtr () noexcept
{
	if (m_ElementsPtrs.Num() > ASnakeBase::m_HeadElementIndex)
		return m_ElementsPtrs[ASnakeBase::m_HeadElementIndex];
	return nullptr;
}

void ASnakeBase::AppendElementOnFoodEating ()
{
	AppendElement(m_LatestWorldLocationOfLastElementInUu);

	check(::IsValid(m_ElementsPtrs.Last()));

	// we'll show the appended item a bit later;
	// otherwise it feels like the snake freezes in place for some time after eating:
	m_ElementsPtrs.Last()->SetActorHiddenInGame(true);
	GetWorldTimerManager().SetTimerForNextTick([this]()
	{
		m_ElementsPtrs.Last()->SetActorHiddenInGame(false);
	});
}

void ASnakeBase::DestroyThisElementAndAllElementsUpToTail (ASnakeElementBase *ElementPtr)
{
	if (ElementPtr == nullptr) // ::IsValid(ElementPtr) is not suitable as the "pending kill" flag doesn't matter here
		return;

	if (ElementPtr->IsHead())
	{
		DestroyAndEndGame();
		return;
	}

	const int32 ElementIndex = m_ElementsPtrs.Find(ElementPtr);
	check(ElementIndex > ASnakeBase::m_HeadElementIndex);

	for (int32 i = ElementIndex; i < m_ElementsPtrs.Num(); ++i)
	{
		ASnakeElementBase *NextElementPtr = m_ElementsPtrs[i];
		check(::IsValid(NextElementPtr));
		NextElementPtr->Destroy();
	}

	constexpr bool IsCapacityPreserved = true;
	m_ElementsPtrs.SetNum(ElementIndex, !IsCapacityPreserved);
}

void ASnakeBase::SetMovementDirection (const EMovementDirection Direction)
{
	if (::IsVerticalDirection(m_MovementDirection) != ::IsVerticalDirection(Direction))
		m_MovementDirection = Direction;
}

void ASnakeBase::SetFrozen (const bool IsFrozen)
{
	m_IsFrozen = IsFrozen;
}

bool ASnakeBase::DoesBoxIntersectBoundingBoxOfAnyElement (const FBox &BoxInUu) const
{
	for (const ASnakeElementBase *ElementPtr : m_ElementsPtrs)
	{
		check(::IsValid(ElementPtr));

		if (ElementPtr->DoesBoxIntersectElementBoundingBox(BoxInUu))
			return true;
	}

	return false;
}

void ASnakeBase::DestroyAndEndGame ()
{
	//TODO: endgame event
	if (GEngine != nullptr)
	{
		constexpr float MessageDurationInS = 5;
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, MessageDurationInS, FColor::Red, TEXT("GAME OVER"));
	}

	this->Destroy();
}

void ASnakeBase::HandleElementOverlapBegin (ASnakeElementBase *ElementPtr, AActor *OtherActorPtr)
{
	const auto InteractableOtherPtr = ::Cast<IInteractable>(OtherActorPtr);
	if (InteractableOtherPtr != nullptr)
		InteractableOtherPtr->StartInteraction(ElementPtr);
}

void ASnakeBase::HandleElementOverlapEnd (ASnakeElementBase *ElementPtr, AActor *OtherActorPtr)
{
	const auto InteractableOtherPtr = ::Cast<IInteractable>(OtherActorPtr);
	if (InteractableOtherPtr != nullptr)
		InteractableOtherPtr->EndInteraction(ElementPtr);
}

void ASnakeBase::HandleElementHit (ASnakeElementBase *ElementPtr, AActor* /*OtherActorPtr*/)
{
	if (::IsValid(ElementPtr) && ElementPtr->IsHead())
		DestroyAndEndGame();
}

void ASnakeBase::Tick (float DeltaTimeInS)
{
	Super::Tick(DeltaTimeInS);

	Move();
}

void ASnakeBase::BeginPlay ()
{
	Super::BeginPlay();

	// we want the movement to be discrete, i.e. each frame the snake must move exactly 'm_ElementSizeInUu':
	this->SetActorTickInterval(m_ElementSizeInUu / m_MovementSpeedInUuPerS);

	m_ElementsPtrs.Empty();

	for (int i = 0; i < m_NumStartingElements; ++i)
	{
		const FVector RelativeLocationOfNewElementInUu(float(m_ElementsPtrs.Num()) * m_ElementSizeInUu, 0.0f, 0.0f);
		const FVector NewElementLocationInUu = this->GetActorLocation() - RelativeLocationOfNewElementInUu;
		// as +X is "up" ("forward"), and the first element (the head) is placed at this->GetActorLocation(),
		// other elements must be behind the head, i.e. they must have smaller X-coordinates

		AppendElement(NewElementLocationInUu);
	}
}

void ASnakeBase::Destroyed ()
{
	// AActor::Destroy() doesn't destroy child actors

	for (ASnakeElementBase *ElementPtr : m_ElementsPtrs)
	{
		check(::IsValid(ElementPtr));
		ElementPtr->Destroy();
	}

	m_ElementsPtrs.Empty();
}

void ASnakeBase::AppendElement (const FVector &TargetWorldLocationInUu)
{
	UWorld *WorldPtr = this->GetWorld();
	check(::IsValid(WorldPtr));

	FActorSpawnParameters NewElementSpawnParams;
	NewElementSpawnParams.Owner = this;
	NewElementSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	auto NewElementPtr =
		WorldPtr->SpawnActor<ASnakeElementBase>(
			m_ElementClass, FTransform(TargetWorldLocationInUu), NewElementSpawnParams);
	check(::IsValid(NewElementPtr));

	const int32 NewElementIndex = m_ElementsPtrs.Add(NewElementPtr);
	if (NewElementIndex == ASnakeBase::m_HeadElementIndex)
		NewElementPtr->MarkAsHead();

	m_LatestWorldLocationOfLastElementInUu = TargetWorldLocationInUu;
}

void ASnakeBase::Move ()
{
	check((m_ElementsPtrs.Num() < 1) || ::IsValid(m_ElementsPtrs.Last()));
	check(this->IsActorTickEnabled());

	if (m_IsFrozen)
		return;

	if (m_ElementsPtrs.Num() < 1)
		return;

	const float DeltaMovementAlongAxisInUu = m_MovementSpeedInUuPerS * this->GetActorTickInterval();
	FVector DeltaMovementInUu = FVector::ZeroVector;

	switch (m_MovementDirection)
	{
	case EMovementDirection::Up:
		DeltaMovementInUu.X += DeltaMovementAlongAxisInUu;
		break;
	case EMovementDirection::Down:
		DeltaMovementInUu.X -= DeltaMovementAlongAxisInUu;
		break;
	case EMovementDirection::Left:
		DeltaMovementInUu.Y -= DeltaMovementAlongAxisInUu;
		break;
	case EMovementDirection::Right:
		DeltaMovementInUu.Y += DeltaMovementAlongAxisInUu;
		break;
	default:
		UE_ASSUME(0);
	}

	ASnakeElementBase *HeadElementPtr = this->HeadElementPtr();
	check(::IsValid(HeadElementPtr));

	FVector PrevElementLocation = HeadElementPtr->GetActorLocation();

	const bool IsHeadElementSweeping = true; // to check for blocking hits
	HeadElementPtr->AddActorWorldOffset(DeltaMovementInUu, IsHeadElementSweeping);

	if (this->IsPendingKillOrUnreachable())
	{
		// being here means that "HeadElementPtr->AddActorWorldOffset()" has lead to destruction of 'this'
		return;
	}

	for (int32 i = ASnakeBase::m_HeadElementIndex + 1; i < m_ElementsPtrs.Num(); ++i)
	{
		ASnakeElementBase *ElementPtr = m_ElementsPtrs[i];
		check(::IsValid(ElementPtr));

		const FVector CurrentElementLocation = ElementPtr->GetActorLocation();

		ElementPtr->SetActorLocation(PrevElementLocation);

		PrevElementLocation = CurrentElementLocation;
	}

	if (m_ElementsPtrs.Num() < 1)
	{
		if (GEngine != nullptr)
		{
			constexpr float MessageDurationInS = 10;
			GEngine->AddOnScreenDebugMessage(
				INDEX_NONE, MessageDurationInS, FColor::Red,
				TEXT("It seems that snake elements are too big and so always overlap each other"));
		}
		return;
	}
	m_LatestWorldLocationOfLastElementInUu = m_ElementsPtrs.Last()->GetActorLocation();
}
