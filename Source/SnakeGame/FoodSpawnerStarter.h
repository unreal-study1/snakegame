#pragma once

#include "Containers/Array.h"
#include "Engine/TriggerBox.h"
#include "UObject/ObjectMacros.h"
#include "UObject/SparseDelegate.h"

#include "Interactable.h"
#include "FoodSpawnerStarter.generated.h"

class AFoodSpawner;
class ASnakeBase;

DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE_TwoParams(FFoodSpawnerStarterWithSnakeHeadInteractionSignature, AFoodSpawnerStarter, OnInteractedWithSnakeHead, AFoodSpawnerStarter*, FoodSpawnerStarter, ASnakeBase*, Snake);

UCLASS()
class SNAKEGAME_API AFoodSpawnerStarter : public ATriggerBox, public IInteractable
{
	GENERATED_BODY()
	
public:
	AFoodSpawnerStarter ();
	~AFoodSpawnerStarter () override = default;

	UFUNCTION(BlueprintCallable, meta = (ToolTip = "Adds another food spawner to the list of spawners which will be activated\nwhen the player interacts with this starter.\n\nDoes nothing if 'Spawner' is empty or was added before"))
	void AddFoodSpawner (
		UPARAM(meta = (DisplayName = "Spawner"))
		AFoodSpawner *SpawnerPtr);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Is Turned On", ToolTip = "If 'false' then no food spawner will be activated on interaction with the player.\n\nIt automatically switchs to 'false' after interacting with the player"))
	bool m_IsTurnedOn = true;

	// called when a head of a snake interacts with 'this'
	UPROPERTY(BlueprintAssignable, Category = "Collision")
	FFoodSpawnerStarterWithSnakeHeadInteractionSignature OnInteractedWithSnakeHead; // note: we cannot use "m_" here

protected:
	void StartInteraction (AActor *InteractorPtr) override;

private:
	TArray<AFoodSpawner*> m_FoodSpawnersPtrs;
};
