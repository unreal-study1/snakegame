#pragma once

#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"

#include "Interactable.h"
#include "Food.generated.h" // must always be the last

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	AFood ();
	~AFood () override = default;

protected:
	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

	void StartInteraction (AActor *InteractorPtr) override;
};
