#pragma once

#include "Delegates/DelegateCombinations.h"
#include "Engine/EngineTypes.h"
#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"

#include "LevelDoor.generated.h"

class ASnakeBase;

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ALevelDoor : public AActor
{
	GENERATED_BODY()
	
public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEventSignature, ALevelDoor*, LevelDoor);

public:
	ALevelDoor ();
	~ALevelDoor () override = default;

	UFUNCTION(BlueprintCallable, meta = (ToolTip = "It assumes that the player is at the doorway,\nso it waits for him to clear the way,\nand only then starts closing"))
	void StartClosingAfterWaitingForPlayerPassage (
		UPARAM(meta = (DisplayName = "Player Pawn"))
		const ASnakeBase *PlayerPawnPtr);

	UFUNCTION(BlueprintCallable)
	void StartOpening ();

	// note: it is called every frame
	void Tick (float DeltaTimeInS) override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (DisplayName = "Mesh", ToolTip = ""))
	UStaticMeshComponent *m_MeshPtr = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Top Z on Closing Animation End, [uu]", ToolTip = "It specifies max possible Z-coordinate of the top side of this door on closing"))
	float m_TopZonClosingAnimationEndInUu = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Bottom Z on Opening Animation End, [uu]", ToolTip = "It specifies min possible Z-coordinate of the bottom side of this door on opening"))
	float m_BottomZonOpeningAnimationEndInUu = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Animation Speed, [uu/s]", ToolTip = "Opening/closing animation speed.\n\"Ease out\" time curve is used, so this speed will be scaled\nby distance to target Z-coordinate"))
	float m_AnimationSpeedInUuPerS = 1.0f;

	// called when this door starts/finihes opening
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "On Started Opening"))
	FEventSignature m_OnStartedOpening;
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "On Finished Opening"))
	FEventSignature m_OnFinishedOpening;

	// called when this door starts/finihes closing
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "On Started Closing"))
	FEventSignature m_OnStartedClosing;
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "On Finished Closing"))
	FEventSignature m_OnFinishedClosing;

protected:
	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

	// note: it is called when the game ends or when the actor is destroyed
	void EndPlay (const EEndPlayReason::Type EndPlayReason) override;

private:
	enum class State
	{
		Idle,
		ClosingWait,
		Closing,
		Opening
	};

private:
	// note: it does nothing if current state is not 'ClosingWait'
	void WaitForPlayerPassageBeforeClosing ();

	void StopClosingWaitingTimer ();

private:
	State m_State = State::Idle;
	const ASnakeBase *m_PtrToPlayerPawnToWaitBeforeClosing = nullptr;
	FTimerHandle m_ClosingWaitingTimerHandle;

private:
	static constexpr float m_ClosingWaitingTimerIntervalInS = 0.3f;
};
