#include "PlayerPawnBase.h"

#include "SnakeBase.h"
#include "SnakeElementBase.h"

#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Misc/AssertionMacros.h"
#include "Templates/Casts.h"

APlayerPawnBase::APlayerPawnBase ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame

	m_SpringArmPtr = this->CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	check(::IsValid(m_SpringArmPtr));

	m_SpringArmPtr->bUsePawnControlRotation = false;

	this->SetRootComponent(m_SpringArmPtr);

	m_CameraPtr = this->CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	check(::IsValid(m_CameraPtr));

	m_CameraPtr->AttachToComponent(m_SpringArmPtr, FAttachmentTransformRules::KeepRelativeTransform);
}

void APlayerPawnBase::SetCameraPitchInDeg (const float Pitch)
{
	if (m_CameraPitchInDeg != Pitch)
	{
		m_CameraPitchInDeg = Pitch;
		UpdateRotation();
	}
}

void APlayerPawnBase::Freeze ()
{
	if (::IsValid(m_SnakeActorPtr))
	{
		m_SnakeActorPtr->SetFrozen(true);
		m_SnakeActorPtr->DisableInput(::Cast<APlayerController>(this->GetController()));
	}
}

void APlayerPawnBase::Unfreeze ()
{
	if (::IsValid(m_SnakeActorPtr))
	{
		m_SnakeActorPtr->SetFrozen(false);
		m_SnakeActorPtr->EnableInput(::Cast<APlayerController>(this->GetController()));
	}
}

void APlayerPawnBase::SetupPlayerInputComponent (UInputComponent *PlayerInputComponentPtr)
{
	check(::IsValid(PlayerInputComponentPtr));

	Super::SetupPlayerInputComponent(PlayerInputComponentPtr);

	PlayerInputComponentPtr->BindAxis(TEXT("Vertical"), this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponentPtr->BindAxis(TEXT("Horizontal"), this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::BeginPlay ()
{
	check(::IsValid(m_SpringArmPtr));

	Super::BeginPlay();

	UpdateRotation();

	CreateSnakeActor();
	check(::IsValid(m_SnakeActorPtr));

	ASnakeElementBase *SnakeHeadElementPtr = m_SnakeActorPtr->HeadElementPtr();
	check(::IsValid(SnakeHeadElementPtr)); // as m_SnakeActorPtr->BeginPlay() was already called (by the engine)

	m_SpringArmPtr->AttachToComponent(
		SnakeHeadElementPtr->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
}

void APlayerPawnBase::CreateSnakeActor ()
{
	UWorld *WorldPtr = this->GetWorld();
	check(::IsValid(WorldPtr));

	m_SnakeActorPtr = WorldPtr->SpawnActor<ASnakeBase>(m_SnakeActorClass, FTransform());
}

void APlayerPawnBase::UpdateRotation ()
{
	this->SetActorRotation(FRotator(GetCameraPitchInDeg(), 0.0f, 0.0f));
	// e.g. if GetCameraPitchInDeg() returns -90, then the camera looks down along Z-axis
}

void APlayerPawnBase::HandlePlayerVerticalInput (const float InputValue)
{
	if (::IsValid(m_SnakeActorPtr))
	{
		if (InputValue > 0.0f)
			m_SnakeActorPtr->SetMovementDirection(EMovementDirection::Up);
		else if (InputValue < 0.0f)
			m_SnakeActorPtr->SetMovementDirection(EMovementDirection::Down);
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput (const float InputValue)
{
	if (::IsValid(m_SnakeActorPtr))
	{
		if (InputValue > 0.0f)
			m_SnakeActorPtr->SetMovementDirection(EMovementDirection::Right);
		else if (InputValue < 0.0f)
			m_SnakeActorPtr->SetMovementDirection(EMovementDirection::Left);
	}
}
