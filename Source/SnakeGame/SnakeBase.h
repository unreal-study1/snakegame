#pragma once

#include "Containers/Array.h"
#include "GameFramework/Actor.h"
#include "Templates/SubclassOf.h"
#include "UObject/ObjectMacros.h"

#include "MovementDirection.h"
#include "SnakeBase.generated.h"

struct FBox;

class ASnakeElementBase;

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:
	// note: by default it has 0 elements
	ASnakeBase ();

	~ASnakeBase () override = default;

	ASnakeElementBase* HeadElementPtr () noexcept;

	void AppendElementOnFoodEating ();

	// warning: 'ElementPtr' must either be nullptr or belong to 'this'
	// note 1: it does nothing if 'ElementPtr' is nullptr
	// note 2: if 'ElementPtr' is the head element, then the whole snake will be destroyed
	void DestroyThisElementAndAllElementsUpToTail (ASnakeElementBase *ElementPtr);

	// warning: it does nothing if e.g. current direction is 'Up', and 'Direction' is 'Down',
	//          i.e. you cannot order the snake to move inside self
	void SetMovementDirection (const EMovementDirection Direction);

	void SetFrozen (const bool IsFrozen);

	bool DoesBoxIntersectBoundingBoxOfAnyElement (const FBox &BoxInUu) const;

	void DestroyAndEndGame ();

	UFUNCTION() // delegate binding requires it
	void HandleElementOverlapBegin (ASnakeElementBase *ElementPtr, AActor *OtherActorPtr);
	UFUNCTION()
	void HandleElementOverlapEnd (ASnakeElementBase *ElementPtr, AActor *OtherActorPtr);
	UFUNCTION()
	void HandleElementHit (ASnakeElementBase *ElementPtr, AActor *OtherActorPtr);

	// note: it is called every frame
	void Tick (float DeltaTimeInS) override;

public:
	UPROPERTY(EditDefaultsOnly, meta = (DisplayName = "Element Class", ToolTip = ""))
	TSubclassOf<ASnakeElementBase> m_ElementClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Num Starting Elements", ClampMin = 1, ToolTip = "Number of elements at the beginning of the game.\nThis number includes the head element"))
	int m_NumStartingElements = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Element Size, [uu]", ClampMin = 0.0f, ToolTip = ""))
	float m_ElementSizeInUu = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Movement Speed, [uu/s]", ClampMin = 0.0f, ToolTip = ""))
	float m_MovementSpeedInUuPerS = 10.0f;

protected:
	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

	void Destroyed () override;

private:
	using Super::Destroy; // clients must use DestroyAndEndGame()

	// note: very first call will create the head
	void AppendElement (const FVector &TargetWorldLocationInUu);

	// note: it does nothing if SetFrozen(false) was called
	void Move ();

private:
	TArray<ASnakeElementBase*> m_ElementsPtrs;

	EMovementDirection m_MovementDirection = EMovementDirection::Up;
	FVector m_LatestWorldLocationOfLastElementInUu = FVector::ZeroVector;
	bool m_IsFrozen = false;

private:
	static constexpr int m_HeadElementIndex = 0;
};
