#include "Food.h"

#include "SnakeBase.h"
#include "SnakeElementBase.h"

#include "Templates/Casts.h"

AFood::AFood ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame
}

void AFood::BeginPlay ()
{
	Super::BeginPlay();
}

void AFood::StartInteraction (AActor *InteractorPtr)
{
	const auto SnakeElementPtr = ::Cast<ASnakeElementBase>(InteractorPtr);
	if (::IsValid(SnakeElementPtr))
	{
		ASnakeBase *SnakePtr = SnakeElementPtr->GetOwnerSnakePtr();
		if (::IsValid(SnakePtr))
		{
			this->Destroy();

			if (SnakeElementPtr->IsHead())
				SnakePtr->AppendElementOnFoodEating();
			else
				SnakePtr->DestroyThisElementAndAllElementsUpToTail(SnakeElementPtr);
		}
	}
}
