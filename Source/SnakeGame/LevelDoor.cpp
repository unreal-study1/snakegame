#include "LevelDoor.h"

#include "SnakeBase.h"
#include "SnakeElementBase.h"

#include "Components/StaticMeshComponent.h"
#include "Engine/Public/TimerManager.h"
#include "Math/UnrealMathUtility.h"
#include "Misc/AssertionMacros.h"

ALevelDoor::ALevelDoor ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame

	m_MeshPtr = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	check(::IsValid(m_MeshPtr));

	m_MeshPtr->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	m_MeshPtr->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	m_MeshPtr->SetGenerateOverlapEvents(false);

	this->SetRootComponent(m_MeshPtr);
}

void ALevelDoor::StartClosingAfterWaitingForPlayerPassage (const ASnakeBase *PlayerPawnPtr)
{
	if ((m_PtrToPlayerPawnToWaitBeforeClosing == PlayerPawnPtr) &&
		((m_State == State::ClosingWait) || (m_State == State::Closing)))
	{
		// nothing to do
		return;
	}

	m_PtrToPlayerPawnToWaitBeforeClosing = PlayerPawnPtr;

	m_State = State::ClosingWait;

	StopClosingWaitingTimer();

	const bool IsLoopingTimer = true;
	this->GetWorldTimerManager().SetTimer(
		m_ClosingWaitingTimerHandle, this, &ALevelDoor::WaitForPlayerPassageBeforeClosing,
		ALevelDoor::m_ClosingWaitingTimerIntervalInS, IsLoopingTimer);
}

void ALevelDoor::StartOpening ()
{
	if (m_State == State::Opening)
		return; // nothing to do

	StopClosingWaitingTimer();

	m_State = State::Opening;

	m_OnStartedOpening.Broadcast(this);
}

void ALevelDoor::Tick (float DeltaTimeInS)
{
	check(::IsValid(m_MeshPtr));

	Super::Tick(DeltaTimeInS);

	const bool IsClosing = (m_State == State::Closing);

	if (IsClosing || (m_State == State::Opening))
	{
		const FBox MeshBoundingBoxInUu = m_MeshPtr->CalcBounds(m_MeshPtr->GetComponentTransform()).GetBox();

		const float CurrentPivotZinUu =
			MeshBoundingBoxInUu.GetCenter().Z +
			(MeshBoundingBoxInUu.GetExtent().Z * (IsClosing? 1.0f : (-1.0f)));

		const float TargetPivotZinUu =
			IsClosing? m_TopZonClosingAnimationEndInUu : m_BottomZonOpeningAnimationEndInUu;

		const float NewPivotZinUu =
			FMath::FInterpTo(CurrentPivotZinUu, TargetPivotZinUu, DeltaTimeInS, m_AnimationSpeedInUuPerS);

		constexpr float TargetProximityInUu = 1.0f;
		// uu is usually cm, and the door is rather far from the camera, so 1cm will be visually indistinguishable
		if (!FMath::IsNearlyEqual(NewPivotZinUu, TargetPivotZinUu, TargetProximityInUu))
		{
			m_MeshPtr->AddWorldOffset(FVector(0, 0, NewPivotZinUu - CurrentPivotZinUu));
		}
		else
		{
			m_MeshPtr->AddWorldOffset(FVector(0, 0, TargetPivotZinUu - CurrentPivotZinUu));

			m_State = State::Idle;

			if (IsClosing)
				m_OnFinishedClosing.Broadcast(this);
			else
				m_OnFinishedOpening.Broadcast(this);
		}
	}
}

void ALevelDoor::BeginPlay ()
{
	Super::BeginPlay();
}

void ALevelDoor::EndPlay (const EEndPlayReason::Type EndPlayReason)
{
	StopClosingWaitingTimer();

	m_State = State::Idle;

	Super::EndPlay(EndPlayReason);
}

void ALevelDoor::WaitForPlayerPassageBeforeClosing ()
{
	if ((m_State != State::ClosingWait) || !::IsValid(m_PtrToPlayerPawnToWaitBeforeClosing))
		return;

	// note: here we assume that the player pawn is above the top side of the door

	constexpr bool AreOnlyCollidingComponentsDefineBounds = true;
	const FBox DoorBoundingBoxInUu = this->GetComponentsBoundingBox(!AreOnlyCollidingComponentsDefineBounds);

	const auto TestBoxTopZinUu = FMath::Max(DoorBoundingBoxInUu.Max.Z, m_TopZonClosingAnimationEndInUu);
	const FBox TestBoxInUu(
		DoorBoundingBoxInUu.Min,
		FVector(DoorBoundingBoxInUu.Max.X, DoorBoundingBoxInUu.Max.Y, TestBoxTopZinUu));

	const bool IsPlayerPawnStillHere =
		m_PtrToPlayerPawnToWaitBeforeClosing->DoesBoxIntersectBoundingBoxOfAnyElement(TestBoxInUu);
	if (!IsPlayerPawnStillHere)
	{
		StopClosingWaitingTimer();
		m_PtrToPlayerPawnToWaitBeforeClosing = nullptr;

		m_State = State::Closing;

		m_OnStartedClosing.Broadcast(this);
	}
}

void ALevelDoor::StopClosingWaitingTimer ()
{
	this->GetWorldTimerManager().ClearTimer(m_ClosingWaitingTimerHandle);
}
