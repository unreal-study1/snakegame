#include "FoodSpawnerStarter.h"

#include "FoodSpawner.h"
#include "SnakeElementBase.h"

#include "Components/SceneComponent.h"
#include "Templates/Casts.h"

AFoodSpawnerStarter::AFoodSpawnerStarter ()
{
	USceneComponent *RootComponentPtr = this->GetRootComponent();
	if (RootComponentPtr != nullptr)
		RootComponentPtr->SetMobility(EComponentMobility::Static);

	this->PrimaryActorTick.bCanEverTick = false;
}

void AFoodSpawnerStarter::AddFoodSpawner (AFoodSpawner *SpawnerPtr)
{
	if (::IsValid(SpawnerPtr))
		static_cast<void>(m_FoodSpawnersPtrs.AddUnique(SpawnerPtr));
}

void AFoodSpawnerStarter::StartInteraction (AActor *InteractorPtr)
{
	if (!m_IsTurnedOn)
		return;

	const auto SnakeElementPtr = ::Cast<ASnakeElementBase>(InteractorPtr);
	if (::IsValid(SnakeElementPtr) && SnakeElementPtr->IsHead())
	{
		for (AFoodSpawner *SpawnerPtr : m_FoodSpawnersPtrs)
		{
			if (::IsValid(SpawnerPtr)) // regardless of the same check in AddFoodSpawner()
				SpawnerPtr->StartSpawningFood(); // possible repeated calls are OK, AFoodSpawner handles it
		}

		OnInteractedWithSnakeHead.Broadcast(this, SnakeElementPtr->GetOwnerSnakePtr());

		m_IsTurnedOn = false;
	}
}
