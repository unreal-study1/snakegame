#pragma once

#include "UObject/Interface.h"
#include "UObject/ObjectMacros.h"

#include "Interactable.generated.h"

class AActor;

// This class does not need to be modified
UINTERFACE(MinimalAPI)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};


class SNAKEGAME_API IInteractable
{
	GENERATED_BODY()

	// TODO: Add interface functions to this class. This is the class that will be inherited to implement this interface

public:
	virtual void StartInteraction (AActor *InteractorPtr) = 0;
	inline virtual void EndInteraction (AActor *InteractorPtr);
};


void IInteractable::EndInteraction (AActor*)
{}
