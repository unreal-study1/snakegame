#include "LevelDoorKeyElement.h"

#include "FloorGridFragment.h"
#include "LevelDoorKey.h"
#include "SnakeElementBase.h"

#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/Public/TimerManager.h"
#include "Math/IntPoint.h"
#include "Misc/AssertionMacros.h"
#include "Templates/Casts.h"

ALevelDoorKeyElement::ALevelDoorKeyElement ()
{
	this->PrimaryActorTick.bCanEverTick = true; // to call Tick() every frame

	m_VisualComponentPtr = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualComponent"));
	check(::IsValid(m_VisualComponentPtr));

	m_VisualComponentPtr->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	m_VisualComponentPtr->SetGenerateOverlapEvents(false);

	this->SetRootComponent(m_VisualComponentPtr);

	m_InteractiveComponentPtr = this->CreateDefaultSubobject<UBoxComponent>(TEXT("InteractiveComponent"));
	check(::IsValid(m_InteractiveComponentPtr));

	m_InteractiveComponentPtr->AttachToComponent(
		m_VisualComponentPtr, FAttachmentTransformRules::KeepRelativeTransform);
	m_InteractiveComponentPtr->SetRelativeScale3D(FVector(1, 1, 1));
}

void ALevelDoorKeyElement::SetInteractiveComponentWorldScaleZ (const float Scale)
{
	const float CorrectScale = FMath::Max(Scale, 0.0f);

	if (!FMath::IsNearlyEqual(m_InteractiveComponentWorldScaleZ, CorrectScale))
	{
		m_InteractiveComponentWorldScaleZ = CorrectScale;
		UpdateInteractiveComponentTransformAlongZaxis();
	}
}

void ALevelDoorKeyElement::SetVisualMesh (UStaticMesh *MeshPtr)
{
	if (m_VisualMeshPtr != MeshPtr)
	{
		m_VisualMeshPtr = MeshPtr;
		UpdateMeshOfVisualComponent();
	}
}

void ALevelDoorKeyElement::SetIdleStateMaterial (UMaterialInterface *MaterialPtr)
{
	SetStateMaterial<State::Idle>(MaterialPtr);
}

void ALevelDoorKeyElement::SetPressedStateMaterial (UMaterialInterface *MaterialPtr)
{
	SetStateMaterial<State::Pressed>(MaterialPtr);
}

void ALevelDoorKeyElement::SetEntireKeyActivatedStateMaterial (UMaterialInterface *MaterialPtr)
{
	SetStateMaterial<State::EntireKeyActivated>(MaterialPtr);
}

void ALevelDoorKeyElement::SetActorSizeToFloorGridCellSizeRatioInIdleState (const float Ratio)
{
	const float CorrectRatio = FMath::Clamp(Ratio, 0.0f, 1.0f);

	if (!FMath::IsNearlyEqual(m_ActorSizeToFloorGridCellSizeRatioInIdleState, CorrectRatio))
	{
		m_ActorSizeToFloorGridCellSizeRatioInIdleState = CorrectRatio;
		AdjustXySizeToFloorGridCell();
	}
}

void ALevelDoorKeyElement::SetActorSizeToFloorGridCellSizeRatioInPressedState (const float Ratio)
{
	const float CorrectRatio = FMath::Clamp(Ratio, 0.0f, 1.0f);

	if (!FMath::IsNearlyEqual(m_ActorSizeToFloorGridCellSizeRatioInPressedState, CorrectRatio))
	{
		m_ActorSizeToFloorGridCellSizeRatioInPressedState = CorrectRatio;
		AdjustXySizeToFloorGridCell();
	}
}

void ALevelDoorKeyElement::MarkAsMain_Implementation ()
{
	m_IsMain = true;
}

void ALevelDoorKeyElement::SnapToFloorGrid (const AFloorGridFragment *GridPtr)
{
	check(::IsValid(m_VisualComponentPtr));

	if (!::IsValid(GridPtr))
		return;

	const FVector VisualComponentCenterInUu =
		ALevelDoorKeyElement::ComputeBoundingBoxInUu(*m_VisualComponentPtr).GetCenter();
	const FIntPoint ClosestFloorCellIndex = GridPtr->FindIndexOfCellContainingPoint(VisualComponentCenterInUu);

	const FVector DesiredBottomCenterLocationInUu =
		GridPtr->ComputeCellCenterInUu(ClosestFloorCellIndex.X, ClosestFloorCellIndex.Y) + FVector::UpVector;
	const float CurrentBottomZinUu = GetBoundingBoxInUu().Min.Z;

	this->AddActorWorldOffset(
		DesiredBottomCenterLocationInUu - FVector(FVector2D(VisualComponentCenterInUu), CurrentBottomZinUu));

	m_SnappedFloorGridCellHalfSizeInUu = (GridPtr->GetGridCellSizeInUu() - GridPtr->GetLineThicknessInUu()) * 0.5f;
	AdjustXySizeToFloorGridCell();
}

void ALevelDoorKeyElement::EnterEntireKeyActivatedState ()
{
	SetState(State::EntireKeyActivated);
}

void ALevelDoorKeyElement::LeaveEntireKeyActivatedState ()
{
	if (m_State == State::EntireKeyActivated)
		SetState(State::Pressed);
}

void ALevelDoorKeyElement::PostRegisterAllComponents ()
{
	Super::PostRegisterAllComponents();

	// note: actual component attachment can be postponed to USceneComponent::OnRegister()
	//       (see the 1st comment there)

	UpdateUnscaledSizeOfInteractiveComponent();
	UpdateInteractiveComponentTransformAlongZaxis();
}

void ALevelDoorKeyElement::BeginPlay ()
{
	Super::BeginPlay();

	UpdateMeshOfVisualComponent();
	UpdateMaterialOfVisualComponent();

	UpdateUnscaledSizeOfInteractiveComponent();
	UpdateInteractiveComponentTransformAlongZaxis();
}

void ALevelDoorKeyElement::StartInteraction (AActor *InteractorPtr)
{
	if (this->IsHidden())
		return;

	if (m_State == State::EntireKeyActivated)
		return;

	const auto SnakeElementPtr = ::Cast<ASnakeElementBase>(InteractorPtr);

	if (!::IsValid(SnakeElementPtr))
		return;

	SetState(State::Pressed);

	if (IsMain() && SnakeElementPtr->IsHead())
	{
		const auto OwnerKeyPtr = ::Cast<ALevelDoorKey>(this->GetOwner());
		if (OwnerKeyPtr != nullptr)
		{
			// we have to defer the call after all key elements update their states;
			// without the deferring there can be a situation when one of key elements
			// is going to enter the 'Idle' state, but now all elements seem to be in the 'Pressed' state,
			// so we would mistakenly enter the 'EntireKeyActivated' state
			FTimerManager &TimerManager = this->GetWorldTimerManager();
			static_cast<void>(
				TimerManager.SetTimerForNextTick(OwnerKeyPtr, &ALevelDoorKey::ActivateKeyIfAllElementsArePressed));
		}
	}
}

void ALevelDoorKeyElement::EndInteraction (AActor *InteractorPtr)
{
	if (this->IsHidden())
		return;

	if (m_State == State::EntireKeyActivated)
		return;

	const auto SnakeElementPtr = ::Cast<ASnakeElementBase>(InteractorPtr);
	if (::IsValid(SnakeElementPtr))
		SetState(State::Idle);
}

#if WITH_EDITOR
void ALevelDoorKeyElement::PostEditChangeProperty (FPropertyChangedEvent &Event)
{
	Super::PostEditChangeProperty(Event);

	if (Event.Property == nullptr)
		return;

	const FName PropertyName = Event.Property->GetFName();

	if (PropertyName == GET_MEMBER_NAME_CHECKED(ALevelDoorKeyElement, m_InteractiveComponentWorldScaleZ))
	{
		// unfortunately SetInteractiveComponentWorldScaleZ() is not called automatically
		// even if it is the BlueprintSetter for 'm_InteractiveComponentWorldScaleZ' property;
		// changing through editor goes this path (through PostEditChangeProperty())
		UpdateInteractiveComponentTransformAlongZaxis();
	}
	else if (PropertyName == GET_MEMBER_NAME_CHECKED(ALevelDoorKeyElement, m_VisualMeshPtr))
	{
		UpdateMeshOfVisualComponent();
	}
	else if ((PropertyName == GET_MEMBER_NAME_CHECKED(ALevelDoorKeyElement, m_IdleStateMaterialPtr)) ||
			 (PropertyName == GET_MEMBER_NAME_CHECKED(ALevelDoorKeyElement, m_PressedStateMaterialPtr)) ||
			 (PropertyName == GET_MEMBER_NAME_CHECKED(ALevelDoorKeyElement, m_EntireKeyActivatedStateMaterialPtr)))
	{
		UpdateMaterialOfVisualComponent();
	}
	else if (PropertyName ==
			 GET_MEMBER_NAME_CHECKED(ALevelDoorKeyElement, m_ActorSizeToFloorGridCellSizeRatioInIdleState))
	{
		AdjustXySizeToFloorGridCell();
	}
	else if (PropertyName ==
			 GET_MEMBER_NAME_CHECKED(ALevelDoorKeyElement, m_ActorSizeToFloorGridCellSizeRatioInPressedState))
	{
		AdjustXySizeToFloorGridCell();
	}
}
#endif

FBox ALevelDoorKeyElement::ComputeBoundingBoxInUu (const USceneComponent &Component)
{
	return Component.CalcBounds(Component.GetComponentToWorld()).GetBox();
}

void ALevelDoorKeyElement::UpdateUnscaledSizeOfInteractiveComponent ()
{
	check(::IsValid(m_InteractiveComponentPtr));
	check(::IsValid(m_VisualComponentPtr));

	const FVector ScaledExtentOfVisualComponentInUu =
		ALevelDoorKeyElement::ComputeBoundingBoxInUu(*m_VisualComponentPtr).GetExtent();
	const FVector VisualComponentScale3D = m_VisualComponentPtr->GetComponentScale();

	FVector UnscaledExtentOfInteractiveComponentInUu = ScaledExtentOfVisualComponentInUu / VisualComponentScale3D;
	UnscaledExtentOfInteractiveComponentInUu.Z = 0.5f;

	m_InteractiveComponentPtr->SetBoxExtent(UnscaledExtentOfInteractiveComponentInUu);
	// now we can use only Set<...>Scale3D() to set the size of the interactive component
}

void ALevelDoorKeyElement::UpdateInteractiveComponentTransformAlongZaxis ()
{
	check(::IsValid(m_InteractiveComponentPtr));
	check(::IsValid(m_VisualComponentPtr));

	FVector WorldScale3D = m_InteractiveComponentPtr->GetComponentScale();
	WorldScale3D.Z = m_InteractiveComponentWorldScaleZ;
	m_InteractiveComponentPtr->SetWorldScale3D(WorldScale3D);

	const auto InteractiveItemHalfSizeZinUu =
		ALevelDoorKeyElement::ComputeBoundingBoxInUu(*m_InteractiveComponentPtr).GetExtent().Z;

	const FBox MeshBoundingBoxInUu = ALevelDoorKeyElement::ComputeBoundingBoxInUu(*m_VisualComponentPtr);
	const auto MeshTopZinUu = MeshBoundingBoxInUu.GetCenter().Z + MeshBoundingBoxInUu.GetExtent().Z;

	FVector LocationInUu = m_InteractiveComponentPtr->GetComponentLocation();
	LocationInUu.Z = MeshTopZinUu + InteractiveItemHalfSizeZinUu;
	m_InteractiveComponentPtr->SetWorldLocation(LocationInUu);
}

FBox ALevelDoorKeyElement::GetBoundingBoxInUu () const
{
	FVector CenterInUu;
	FVector HalfSizesInUu;
	constexpr bool AreOnlyCollidingComponentsDefineBounds = false;

	this->GetActorBounds(AreOnlyCollidingComponentsDefineBounds, CenterInUu, HalfSizesInUu);

	return FBox(CenterInUu - HalfSizesInUu, CenterInUu + HalfSizesInUu);
}

void ALevelDoorKeyElement::SetState (const State State)
{
	if (m_State == State)
		return;

	m_State = State;

	UpdateMaterialOfVisualComponent();
	AdjustXySizeToFloorGridCell();
}

template<ALevelDoorKeyElement::State State>
void ALevelDoorKeyElement::SetStateMaterial (UMaterialInterface *MaterialPtr)
{
	UMaterialInterface* &StoredMaterialPtr = StateMaterialPtr<State>(); // warning: '&' is important

	if (StoredMaterialPtr != MaterialPtr)
	{
		StoredMaterialPtr = MaterialPtr;
		UpdateMaterialOfVisualComponent();
	}
}

UMaterialInterface* ALevelDoorKeyElement::GetMaterialPtrForCurrentState () const noexcept
{
	switch (m_State)
	{
	case State::Idle:
		return m_IdleStateMaterialPtr;
	case State::Pressed:
		return m_PressedStateMaterialPtr;
	case State::EntireKeyActivated:
		return m_EntireKeyActivatedStateMaterialPtr;
	default:
		UE_ASSUME(0);
	}
}

void ALevelDoorKeyElement::UpdateMeshOfVisualComponent ()
{
	check(::IsValid(m_VisualComponentPtr));

	if (m_VisualComponentPtr->GetStaticMesh() != m_VisualMeshPtr)
	{
		m_VisualComponentPtr->SetStaticMesh(m_VisualMeshPtr);

		UpdateMaterialOfVisualComponent();

		UpdateUnscaledSizeOfInteractiveComponent();
		UpdateInteractiveComponentTransformAlongZaxis();
	}
}

void ALevelDoorKeyElement::UpdateMaterialOfVisualComponent ()
{
	check(::IsValid(m_VisualComponentPtr));

	m_VisualComponentPtr->SetMaterial(0, GetMaterialPtrForCurrentState());
}

void ALevelDoorKeyElement::AdjustXySizeToFloorGridCell ()
{
	if (m_SnappedFloorGridCellHalfSizeInUu < 0.0f)
		return;

	float ActorSizeToCellSizeRatio = m_ActorSizeToFloorGridCellSizeRatioInPressedState;
	if (m_State == State::Idle)
		ActorSizeToCellSizeRatio = m_ActorSizeToFloorGridCellSizeRatioInIdleState;

	const float DesiredXyHalfSizeInUu = m_SnappedFloorGridCellHalfSizeInUu * ActorSizeToCellSizeRatio;
	const FVector CurrentHalfSizesInUu = GetBoundingBoxInUu().GetExtent();

	FVector Scale3D = this->GetActorScale3D();
	Scale3D.X *= (DesiredXyHalfSizeInUu / CurrentHalfSizesInUu.X);
	Scale3D.Y *= (DesiredXyHalfSizeInUu / CurrentHalfSizesInUu.Y);
	this->SetActorScale3D(Scale3D);
}
