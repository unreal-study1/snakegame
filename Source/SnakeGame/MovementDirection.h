#pragma once

#include "UObject/ObjectMacros.h"

UENUM()
enum class EMovementDirection
{
	Up,
	Down,
	Left,
	Right
};


inline bool IsVerticalDirection (const EMovementDirection Direction) noexcept
{
	return ((Direction == EMovementDirection::Up) || (Direction == EMovementDirection::Down));
}

inline bool IsHorizontalDirection (const EMovementDirection Direction) noexcept
{
	return ((Direction == EMovementDirection::Left) || (Direction == EMovementDirection::Right));
}
