#pragma once

#include "GameFramework/Actor.h"
#include "Math/Vector2D.h"
#include "Math/Vector.h"
#include "UObject/ObjectMacros.h"

#include "FloorGridFragment.generated.h"

struct FIntPoint;
class UMaterialInstance;
class UStaticMeshComponent;

// Note: it uses an instance of XyGridMaterial, which operates in world coordinates
//       (i.e. this material is object- and screen-independent)
UCLASS()
class SNAKEGAME_API AFloorGridFragment : public AActor
{
	GENERATED_BODY()
	
public:	
	AFloorGridFragment ();
	~AFloorGridFragment () override = default;

	UFUNCTION(BlueprintCallable, Category = "Utilities|Transformation")
	void SetLocationZ (const float ZinUu);

	// warning: 'MaterialPtr' must point to an instance of XyGridMaterial
	UFUNCTION(BlueprintCallable, Category = "Construction")
	void SetXyGridMaterial (UMaterialInstance *MaterialPtr);

	// note: result pointer (if valid) will point to an instance of XyGridMaterial
	UFUNCTION(BlueprintCallable, Category = "Construction")
	const UMaterialInstance* GetXyGridMaterial () const;

	// note: in case of error they return a negative value
	float GetGridCellSizeInUu () const;
	float GetLineThicknessInUu () const;

	// note 1: it doesn't check if 'CellIndexAlongWorldX' ('CellIndexAlongWorldY') is nonnegative or
	//         is less than 'm_NumCellsAlongWorldX' ('m_NumCellsAlongWorldY'),
	//         so the result can be outside of this grid fragment
	// note 2: Z-component of result vector will be equal to "this->GetActorLocation().Z"
	FVector ComputeCellCenterInUu (const int32 CellIndexAlongWorldX, const int32 CellIndexAlongWorldY) const;

	// note: see notes for the overload above
	inline FVector ComputeCellCenterInUu (const uint32 CellIndexAlongWorldX, const uint32 CellIndexAlongWorldY) const;

	FIntPoint FindIndexOfCellContainingPoint (const FVector2D &PointInUu) const;

	// note: it behaves the same as the overload above, simply ignoring Z component of 'PointInUu'
	FIntPoint FindIndexOfCellContainingPoint (const FVector &PointInUu) const;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Location & Size", meta = (DisplayName = "Offset from World Origin, cells", ToolTip = "Offset from World Origin in cells.\nOnly integers and halfs make sense here"))
	FVector2D m_OffsetFromWorldOriginXyInCells = FVector2D::ZeroVector; // float is because half-cell is also supported

	UPROPERTY(EditAnywhere, Category = "Location & Size", meta = (DisplayName = "Size along X-axis, cells", ToolTip = "Size along X-axis in cells", ClampMin = 1))
	uint32 m_NumCellsAlongWorldX = 1;
	UPROPERTY(EditAnywhere, Category = "Location & Size", meta = (DisplayName = "Size along Y-axis, cells", ToolTip = "Size along Y-axis, in cells", ClampMin = 1))
	uint32 m_NumCellsAlongWorldY = 1;

protected:
	// note: it is called when the game starts or when the actor is spawned
	void BeginPlay () override;

#if WITH_EDITOR
	// note: it is called when scalar property, marked with UPROPERTY, changes in the editor
	void PostEditChangeProperty (FPropertyChangedEvent &Event) override;
#endif

private:
	static float FixOffsetFromWorldOriginInCells (const float RawOffsetInCells);

	static float ComputeGridOffsetFromWorldOriginInUu (
		const float RelativeGridOffsetFromWorldOriginInPercent, const float GridCellSizeInUu);

private:
	void UpdateXyLocationAndScale ();

	// note: if GetXyGridMaterial() returns nullptr,
	//       or if the resulting material doesn't have a param named 'ParamName',
	//       then the method logs the error and returns a negative value
	float GetFloatParamOfXyGridMaterial (const TCHAR *ParamName) const;

	// note: in case of error it returns a negative value
	float GetRelativeGridOffsetFromWorldOriginInPercent () const;

	// note: "bottom left" instead of "top left" is because in UE
	//       plane X- and Y-axes go to the right and to the top
	FVector ComputeBottomLeftCellLocationInUu () const;

private:
	UStaticMeshComponent *m_MeshComponentPtr = nullptr;
	FVector m_MeshSizesInUu = FVector::ZeroVector;

private:
	static const TCHAR *m_XyGridMaterialParamNameForCellSizeInUu;
	static const TCHAR *m_XyGridMaterialParamNameForLineThicknessInUu;
	static const TCHAR *m_XyGridMaterialParamNameForRelativeOffsetFromWorldOriginInPercent;
};


FVector AFloorGridFragment::ComputeCellCenterInUu (
	const uint32 CellIndexAlongWorldX, const uint32 CellIndexAlongWorldY) const
{
	return ComputeCellCenterInUu(int32(CellIndexAlongWorldX), int32(CellIndexAlongWorldY));
}
